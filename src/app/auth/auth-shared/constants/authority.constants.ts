export const enum Authority {
    ADMIN = 'ROLE_ADMIN',
    LIDER = 'ROLE_LIDER',
    REGISTRADO = 'ROLE_REGISTRADO'
}
