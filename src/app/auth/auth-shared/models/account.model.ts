export class Account {
    constructor (
        //public activated: boolean,
        public idDetailUser:number,
        public username:string,
        public nombre:string,
        public lastName:number,
        public documentNumber:string,
        public personalMail:number,
        public emailSena:number,
        public rolsUsers:string,
        public activated: boolean,
        public authorities: string[],
    ) {}
}