export interface ICredentials {
    username?: string;
    password?:string;
    grant_type?:string;
    rememberMe?:boolean;
}
export class Credentials implements ICredentials {
    constructor (
        public username?: string,
        public password?: string,
        public grant_type?:string,
        public rememberMe?:boolean
    ) {}
}
const credentials: ICredentials = new Credentials();
