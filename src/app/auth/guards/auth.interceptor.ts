import { Injectable } from '@angular/core';
import { HttpInterceptor, HttpRequest, HttpHandler, HttpEvent } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {
    constructor(){}


    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
        if (
            !request
            || !request.url
            || (!request.url.startsWith('http') && !(environment.END_POINT && request.url.startsWith(environment.END_POINT)))) {
            return next.handle(request);
        }

        const token = localStorage.getItem('access_token') || sessionStorage.getItem('access_token');

        if (token) {
            request = request.clone({
                setHeaders: {
                    Authorization: 'bearer ' + token
                }
            });
        }
        return next.handle(request);
    }
}
