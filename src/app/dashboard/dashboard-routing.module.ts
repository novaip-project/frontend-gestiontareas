import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';

//Inyect of modules neccesary
const routes: Routes = [
  {
    path: '',
    component: MainDashboardComponent,
    children: [
 
      {
        path: 'detail-user',
        loadChildren: () => import('../modules/detail-user/detail-user.module')
          .then(module => module.DetailUserModule)
      },
      {
        path: 'tasks',
        loadChildren: () => import('../modules/task/task.module')
          .then(module => module.TaskModule)
      }

  
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
