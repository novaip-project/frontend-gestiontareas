import { Component, OnInit, Input } from '@angular/core';

import { faBullhorn, faFile, faFlag,faWeight, faHome, faList, faAngleDoubleLeft, faUsers, faUsersCog} from '@fortawesome/free-solid-svg-icons';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.styl']
})
export class AsideComponent implements OnInit {
  faUsers=faUsers;
  faUsersCog = faUsersCog;
faAngleDoubleLeft=faAngleDoubleLeft;
faFlag=faFlag;
faWeight=faWeight;
faBullhorn=faBullhorn;
faFile=faFile;
faHome=faHome;
faList=faList;
//pido recibir un valor
 @Input() isActiveAside: boolean;
  constructor() { }

  ngOnInit(): void {
  }

}
