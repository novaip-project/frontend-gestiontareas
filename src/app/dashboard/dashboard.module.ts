import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { AsideComponent } from './aside/aside.component';
import { MainDashboardComponent } from './main-dashboard/main-dashboard.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import {AuthSharedModule} from "../auth/auth-shared/auth-shared.module";


@NgModule({
  declarations: [
    NavbarComponent,
    MainDashboardComponent,
    AsideComponent,
  ],
  imports: [
    CommonModule,
    DashboardRoutingModule,
    FontAwesomeModule,
    AuthSharedModule
  ]
})
export class DashboardModule { }
