import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-main-dashboard',
  templateUrl: './main-dashboard.component.html',
  styleUrls: ['./main-dashboard.component.styl']
})
export class MainDashboardComponent implements OnInit {

  //Variables
  isActive = false

  constructor(
  ) {

   }

  ngOnInit(): void {
  }
  cambio(valor:boolean): void {
    this.isActive = valor;
  }

}
