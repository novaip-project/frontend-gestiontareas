import { OfferTypeStudy } from './enumerations/offerTypeStudy';
import { TrainingTypeStudy } from './enumerations/trainingTypeStudy';
import { TypeWorkingDay } from './enumerations/typeWorkingDay';
import { IProgram } from './program';

export interface IFicha{
  
    id:number;
    startDate:Date,
    status:Boolean,
    dateEndLective:Date,
    dateEndPractice:Date,
    offerTypeStudy:OfferTypeStudy,
    trainingType:TrainingTypeStudy,
    workingDayType:TypeWorkingDay,
    program:IProgram,
}
export interface IFichaId {
    id:string
}

