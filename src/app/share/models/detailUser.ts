import { GenderType } from './enumerations/genderType';
import { DocumentType } from './enumerations/documentType';
import { IUser } from './users';

export interface IdetailUser{
     
     id?: string;
     documentNumber?: string;
     documentType?:DocumentType;
     expeditionDate?: Date;
     phoneNumber?:Number;
     personalMail?:string;
     emailSena?: string;
     genderType?:GenderType;
     user?: IUser
     statusCallAttention?:boolean   
 
}

