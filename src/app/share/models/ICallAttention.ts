import { IFicha } from './ficha';
import { IdetailUser } from './detailUser';
import { Trimester } from './enumerations/trimester';
import { TypeCall } from './enumerations/typeCall';
export interface ICallAttention {

    idCall?: number,
    reportUserId?: IdetailUser,
    situation?: string,
    descriptionCausal?: string,
    impact?: string,
    effect?: string,
    commitments?: string,
    dateAttention?: Date,
    recuperationTasks?:string,
    suggestionTasks?: string,
    userReporter?: IdetailUser,
    fichas?:IFicha
    trimester?:Trimester,
    statusCallAttention?:boolean,
    typeCall?:TypeCall,

}
