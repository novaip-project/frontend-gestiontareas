export interface IUser {
    idUser?: string,
    username: string,  
    firstName: string,
    lastName: string,
    password: string,
    enabled?:boolean,
    rolsUsers: [
        {
        id: number,
        }
    ],
    authorities?: string[],
    activationDate?: Date,

}
