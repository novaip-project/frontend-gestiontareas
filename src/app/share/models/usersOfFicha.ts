import { TypeRol } from './enumerations/typeRol';

export interface IUsersFicha {
    id?: string,
    idUser?: number,
    fullName?: string,
    rol:TypeRol 
}
