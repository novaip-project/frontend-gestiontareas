export interface IUserFiterDTO {
    id:number
    fullName:string
    idDetailUserReported:number
    documentNumber:string
    nameProgram:string
    nameGestor:string
    code:string
}