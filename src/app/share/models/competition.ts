export interface ICompetition{
    
    id?:number,
    name?:String,
    status?:boolean,
    code?:String,
    durationHours?:string,
}