import { IUser } from './users';
import { IFicha } from './ficha';
import { TypeRol } from './enumerations/typeRol';

export interface ITeamStaff{
     id?:string;
     ficha?:IFicha;
     users?:IUser,
     rol:TypeRol,
     
}
