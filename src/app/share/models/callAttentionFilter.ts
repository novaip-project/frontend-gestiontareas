import { Trimester } from './enumerations/trimester';
import { TypeCall } from './enumerations/typeCall';

export interface ICallFilter{

    reportUserId?: number,
    dateAttention?: Date,
    documentNumber?: string,
    code?: string,
    trimester?: Trimester,
    typeCall?: TypeCall,
    statusCallAttention?: boolean,
    fullNameUser?: string,
    fullNameReporter?: string

}
