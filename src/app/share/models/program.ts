import { ICompetition } from './competition';
import { TrainingMode } from './enumerations/trainingMode';
import { TrainingTypeStudy } from './enumerations/trainingTypeStudy';

export interface IProgram {

    id:number;
    version:string,
    nameProgram:string,
    durationTrimesterLective:string,
    status:boolean,
    justification:string,
    durationTrimesterProduction:string,
    trainingTypeStudy:TrainingTypeStudy,
    trainingMode:TrainingMode,
    levelTraining:string;
    Competitions:ICompetition
}
 


  