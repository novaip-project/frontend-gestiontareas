export interface IUserFilterDocument{
    code: string,
    documentNumber: string,
    fullName: string,
    idDetailUserReported: number,
    idUser: number,
    nameGestor: string,
    nameProgram: string
}

    