import { FormControl } from '@angular/forms'
import { timer } from 'rxjs'
import { switchMap, map } from 'rxjs/operators'

export const callAttentionValidator = (validatorService: any) => {
    return (input: FormControl) => {
        return timer(100).pipe(
            switchMap(() => validatorService.getDocumentNumber({documentNumber: input.value}).pipe(
                map((res: any) => {
                    if (res && res.length > 0 ) {
                        return { exist: true };
                    }
                    return null;
                })
            ))
        );
    };
};