import { UserRouteAccesGuard } from './auth/guards/user.route.acces.guard';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';
import { AccessDeniedComponent } from './auth/access-denied/access-denied.component';
import { Authority } from './auth/auth-shared/constants/authority.constants';

//Inyect of modules neccesary
const routes: Routes = [
  {
    path: 'index',
    data:{
      authorities: [Authority.ADMIN, Authority.REGISTRADO, Authority.LIDER]
    },
    canActivate:[UserRouteAccesGuard],
    loadChildren: () => import('./dashboard/dashboard.module')
      .then(module => module.DashboardModule)
  },
  {
    path: 'login',
    component: LoginComponent
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'accessdenied',
    component: AccessDeniedComponent
  },
];
@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
