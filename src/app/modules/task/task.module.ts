import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TaskListComponent } from './task-list/task-list.component';
import { MainComponentComponent } from './main-component/main-component.component';



@NgModule({
  declarations: [TaskListComponent, MainComponentComponent],
  imports: [
    CommonModule
  ]
})
export class TaskModule { }
