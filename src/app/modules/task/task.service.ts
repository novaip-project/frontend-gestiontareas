
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createRequestParams } from 'src/app/utils/request.utils';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IdetailUser } from '../../share/models/detailUser';


@Injectable({
  providedIn: 'root'
})
export class TaskService {

  constructor(private http: HttpClient) { }

  public getTasksAll(req?: any): Observable<IdetailUser[]> {
    let params = createRequestParams(req);
    return this.http.get<IdetailUser[]>(`${environment.END_POINT}/api/tasks`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  };
  

  public updateDetailUser(IdetailUser: IdetailUser): Observable<IdetailUser>{
    return this.http.put<IdetailUser>(`${environment.END_POINT}/api/detail-user`, IdetailUser)
    .pipe(map(res => {
      return res;
    }));
  }

  public deleteDetailUser(id: string): Observable<IdetailUser>{
    return this.http.delete<IdetailUser>(`${environment.END_POINT}/api/user/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }



}