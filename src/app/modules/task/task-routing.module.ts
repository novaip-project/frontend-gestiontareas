import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MainComponentComponent } from './main-component/main-component.component';
import { TaskListComponent } from './task-list/task-list.component';


const routes: Routes = [
  {
    path: '',
    component: MainComponentComponent,
    children: [
      {
        path: '',
        component: TaskListComponent
      },
      {
        path: 'detail-user-create'
      },
      {
        path: 'update/:id',
      },
      {
        path: 'detail-user-view/:id',
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailUserRoutingModule { }


