import { Component } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DetailUserService } from '../detail-user.service';
import { IdetailUser } from '../../../share/models/detailUser';
import { IRolsUsers } from 'src/app/share/models/rolsUser';
import { Router } from '@angular/router';
import { callAttentionValidator } from 'src/app/share/customValidators/callAttention-validator';
import {NgbDateStruct} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-detail-user-create',
  templateUrl: './detail-user-create.component.html',
  styleUrls: ['./detail-user-create.component.styl']
})

export class DetailUserCreateComponent{

  model: NgbDateStruct;
  rolsUsers: IRolsUsers[] = [];
  detailUser: IdetailUser;
  userFormGroupCreate: FormGroup;
  //form
  detailUserFormGroup: FormGroup;
  formBuilderRol: FormGroup
  userRolsUserForm: FormGroup;
  userFormGroup: FormGroup;
  statusEnabled: true;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private detailUserService: DetailUserService,

  ) {
    this.detailUserFormGroup = this.formBuilder.group({
      documentNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.maxLength(10)
      ]), callAttentionValidator(this.detailUserService)],
      documentType: ['', Validators.required],
      expeditionDate: ['',  Validators.required],
      phoneNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(9),
        Validators.maxLength(10),
      Validators.pattern('^[0-9]+$')])],
      personalMail: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])],
      emailSena: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])],
      genderType: ['', Validators.required],
      user: [''],
    })
    this.userFormGroupCreate = this.formBuilder.group({
      idUser: [''],
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45)
      ]), callAttentionValidator(this.detailUserService)],
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ])],
      enabled: ['', Validators.required],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(70)
      ])],
      rolsUsers: ['', Validators.required]
    });

    this.formBuilderRol = this.formBuilder.group({
      id: [''],
      typeRol: ['', Validators.required]
    })
  }

  ngOnInit() {
  }

  saveUser() {
    this.userFormGroupCreate.value.rolsUsers = this.rolsUsers;
  }

  savedetailUser(): void {
    this.userFormGroupCreate.value.rolsUsers = [
      {
        id: this.formBuilderRol.value.id
      }
    ];
    
    this.detailUserFormGroup.value.user = this.userFormGroupCreate.value;
    this.detailUserService.savedetailUser(this.detailUserFormGroup.value)
      .subscribe(
        res => {
          alert("Se ha guardado con exito")
          this.router.navigate(['../index/detail-user/']);
        }, error => {
        });

  }


}