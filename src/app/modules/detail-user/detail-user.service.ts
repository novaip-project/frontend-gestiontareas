
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { createRequestParams } from 'src/app/utils/request.utils';
import { environment } from 'src/environments/environment';
import { map } from 'rxjs/operators';
import { IdetailUser } from '../../share/models/detailUser';
import { IUserFilterDocument } from 'src/app/share/models/userFilter';
import { IUser } from 'src/app/share/models/users';


@Injectable({
  providedIn: 'root'
})
export class DetailUserService {

  constructor(private http: HttpClient) { }

  public getUsersAll(req?: any): Observable<IdetailUser[]> {
    let params = createRequestParams(req);
    return this.http.get<IdetailUser[]>(`${environment.END_POINT}/api/users`, { params: params })
      .pipe(map(res => {
        return res;
      }));
  };
  
  public getDetailUserById(id:string): Observable<IdetailUser> {

    return this.http.get<IdetailUser>(`${environment.END_POINT}/api/user/${id}`)
      .pipe(map(res => {
        return res;
      }));
    }

 

  public savedetailUser(IdetailUser: IdetailUser): Observable<IdetailUser> {
    return this.http.post<IdetailUser>(`${environment.END_POINT}/api/detail-user`, IdetailUser)
      .pipe(map(res => {
        return res;
      }));
  }

  public updateDetailUser(IdetailUser: IdetailUser): Observable<IdetailUser>{
    return this.http.put<IdetailUser>(`${environment.END_POINT}/api/detail-user`, IdetailUser)
    .pipe(map(res => {
      return res;
    }));
  }

  public deleteDetailUser(id: string): Observable<IdetailUser>{
    return this.http.delete<IdetailUser>(`${environment.END_POINT}/api/user/${id}`)
    .pipe(map(res => {
      return res;
    }));
  }

  public getAccount(username: string): Observable<IUser>{
    const params = createRequestParams(username);
    return this.http.get<IUser>(`${environment.END_POINT}/api/user/account`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public getAccountUser(username: string): Observable<IUser>{
    const params = createRequestParams(username);
    return this.http.get<IUser>(`${environment.END_POINT}/api/user/account`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }

  public getDocumentNumber(documentNumber: string): Observable<IdetailUser>{
    const params = createRequestParams(documentNumber);
    return this.http.get<IdetailUser>(`${environment.END_POINT}/api/detail-user/search`, {params: params})
    .pipe(map(res => {
      return res;
    }));
  }


}