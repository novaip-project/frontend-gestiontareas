import { Component, OnInit } from '@angular/core';
import { DetailUserService } from '../detail-user.service';
import { IdetailUser } from '../../../share/models/detailUser';
import { ActivatedRoute, Router } from '@angular/router';
import { IUser } from 'src/app/share/models/users';
import { ConfirmationService } from 'primeng/api';

@Component({
  selector: 'app-detail-user-view',
  templateUrl: './detail-user-view.component.html',
  styleUrls: ['./detail-user-view.component.styl'],
  providers:[ConfirmationService]
})
export class DetailUserViewComponent implements OnInit 
{
  
  public detailUser: IdetailUser;
  public user: IUser;



  constructor(
    private detailUserService: DetailUserService,    
    private confirmService: ConfirmationService ,
    private activatedRoute:ActivatedRoute, 
    private route:Router
    ) { }
  ngOnInit() {
   
    let id = this.activatedRoute.snapshot.paramMap.get('id');
    this.detailUserService.getDetailUserById(id)
    .subscribe(res => {
      this.detailUser = res;
      this.user = this.detailUser.user
    });

}
sendDataToUser (idUser:string):void{
  this.route.navigate[("/index/user/users-update/', idUser")];
}



}
