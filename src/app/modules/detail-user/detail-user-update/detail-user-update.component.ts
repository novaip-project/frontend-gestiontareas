import { Component, OnInit } from '@angular/core';
import { IdetailUser } from 'src/app/share/models/detailUser';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { DetailUserService } from '../detail-user.service';
import { IUser } from 'src/app/share/models/users';
import { IRolsUsers } from 'src/app/share/models/rolsUser';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detail-user-update',
  templateUrl: './detail-user-update.component.html',
  styleUrls: ['./detail-user-update.component.styl']
})
export class DetailUserUpdateComponent implements OnInit {

  rolsUsers: IRolsUsers[] = [];
  iUser: IUser;
  detailUser: IdetailUser;
  userList: IUser;
  userFormGroupUpdate: FormGroup;
  usersListUpdate: IUser[];
  usersList: IUser
  //form
  detailUserFormGroup: FormGroup;
  formBuilderRol: FormGroup
  userRolsUserForm: FormGroup;
  updateDetailUser: IdetailUser;
  updateUser: IUser;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private detailUserService: DetailUserService,
    private activatedRouter: ActivatedRoute,
  ) {
    this.detailUserFormGroup = this.formBuilder.group({
      documentNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(8),
        Validators.pattern('^[0-9]+$')
      ])],
      documentType: ['', Validators.required],
      expeditionDate: ['', Validators.required],
      phoneNumber: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
        Validators.pattern('^[0-9]+$')
      ])],
      personalMail: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])],
      emailSena: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45),
        Validators.pattern('[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,3}$')
      ])],
      genderType: ['', Validators.required],
      user: [''],
    })
    this.userFormGroupUpdate = this.formBuilder.group({
      idUser: [''],
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(20),
        Validators.maxLength(45)
      ])],
      firstName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ])],
      lastName: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(50)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(5),
        Validators.maxLength(70)
      ])],
      rolsUsers: ['', Validators.required]
    });
    this.formBuilderRol = this.formBuilder.group({
      id: [''],
      typeRol: ['', Validators.required]
    })
  }

  ngOnInit() {
    let id = this.activatedRouter.snapshot.paramMap.get('id');
    this.detailUserService.getDetailUserById(id)
    .subscribe(res => {
      this.updateDetailUser = res;
      this.loadFormDetailUser(res);
    });

    
  }

  private loadFormDetailUser( detailUserLoad: IdetailUser){
    this.detailUserFormGroup.patchValue({
      id: detailUserLoad.id,
      documentNumber: detailUserLoad.documentNumber,
      documentType: detailUserLoad.documentType,
      expeditionDate: detailUserLoad.expeditionDate,
      phoneNumber: detailUserLoad.phoneNumber,
      personalMail: detailUserLoad.personalMail,
      emailSena: detailUserLoad.emailSena,
      genderType: detailUserLoad.genderType,
      user: detailUserLoad.user
    })
    
  }

  private loadFormUser(userLoad: IUser){
    this.userFormGroupUpdate.patchValue({
      idUser: userLoad.idUser,
      login: userLoad.username,
      firstName: userLoad.firstName,
      lastName: userLoad.lastName,
      rolsUsers: userLoad.rolsUsers
    })
  }

  saveUser() {
    //this.typeRol = this.formBuilderRol.value; 
    this.userFormGroupUpdate.value.rolsUsers = this. rolsUsers;
  }

  
  savedetailUser() {
    this.userFormGroupUpdate.value.rolsUsers = [
      {
        typeRol: this.formBuilderRol.value.typeRol
      }
    ]; 
    this.detailUserFormGroup.value.user = this.userFormGroupUpdate.value;

      this.detailUserService.updateDetailUser(this.detailUserFormGroup.value)
    .subscribe(
      res =>{
      alert("Se Ha actualizado Correctamente")
        this.router.navigate(['..index/detail-user']);
      }, error => {
    });
    
      
  }
  

}