import { Component, OnInit } from '@angular/core';
import { DetailUserService } from '../detail-user.service';
import { FormBuilder, FormGroup } from '@angular/forms';
import { IdetailUser } from '../../../share/models/detailUser';
import { faTrash, faPen , faEye} from '@fortawesome/free-solid-svg-icons'
@Component({
  selector: 'app-detail-user-list',
  templateUrl: './detail-user-list.component.html',
  styleUrls: ['./detail-user-list.component.styl']
})
export class DetailUserListComponent implements OnInit {


    //icons
    faTrash=faTrash;
    faPen=faPen;
    faEye=faEye;

    //PAGINACION Atributte
    pageSize = 5;
    pageNumber = 0;
    listPage = [];
    detailUserList: IdetailUser[] = [];
    detailUserSelectTemp: IdetailUser;
    detailUserStatus: boolean = false;
    detailUserRes: IdetailUser[];
  
    detailUserFormGroup:FormGroup;
    statusSearchDocument: boolean = false;
    searchFormDetailUser = this.fb.group({
      documentNumber: ['']
    });
    constructor(
      private detailUserService:DetailUserService,
      private fb : FormBuilder
    ) { }

    ngOnInit() {
      this.loadingPagination
      this.loadingPagination(this.pageNumber, this.pageSize);
      
    }
  // TRAER DATOS POR PAGINACIONS

    loadingPagination( pageNumber: number, pageSize: number): void {
      this.detailUserService.getUsersAll({
        'pageSize': pageSize,
        'pageNumber': pageNumber
      })
        .subscribe((res: any) => {
          this.detailUserList = res.content;
          console.log(this.detailUserList)
          this.formatPagination(res.totalPages);
        });

    }

    private formatPagination(totalPages: number): void{
      this.listPage = [];
        for(let i = 0; i < totalPages; i++){
          this.listPage.push(i);
        }
    }
      
    selectDetailUser(detailUser:IdetailUser){
      this.detailUserSelectTemp = detailUser;
    }
    
   

    deleteDetailUser(id: string){
      this.detailUserService.deleteDetailUser(id)
      .subscribe(res => {
        this.ngOnInit();
      });
    }
}
