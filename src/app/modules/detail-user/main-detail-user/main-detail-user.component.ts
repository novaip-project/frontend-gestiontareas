import { Component, OnInit } from '@angular/core';
import { faPlusCircle, faEye, faList } from '@fortawesome/free-solid-svg-icons'

@Component({
  selector: 'app-main-detail-user',
  templateUrl: './main-detail-user.component.html',
  styleUrls: ['./main-detail-user.component.styl']
})
export class MainDetailUserComponent implements OnInit {

  faPlusCircle=faPlusCircle;
  faStreetView=faEye;
  faList=faList;


  constructor() { }

  ngOnInit(): void {
  }

}
