import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DetailUserCreateComponent } from './detail-user-create/detail-user-create.component';
import { DetailUserListComponent } from './detail-user-list/detail-user-list.component';
import { DetailUserUpdateComponent } from './detail-user-update/detail-user-update.component';
import { DetailUserViewComponent } from './detail-user-view/detail-user-view.component';
import { MainDetailUserComponent } from './main-detail-user/main-detail-user.component';


const routes: Routes = [
  {
    path: '',
    component: MainDetailUserComponent,
    children: [
      {
        path: '',
        component: DetailUserListComponent
      },
      {
        path: 'detail-user-create',
        component: DetailUserCreateComponent
      },
      {
        path: 'update/:id',
        component: DetailUserUpdateComponent
      },
      {
        path: 'detail-user-view/:id',
        component: DetailUserViewComponent
      }
    ]
  }


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DetailUserRoutingModule { }


