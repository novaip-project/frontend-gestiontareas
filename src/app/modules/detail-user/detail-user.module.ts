import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DetailUserCreateComponent } from './detail-user-create/detail-user-create.component';
import { DetailUserListComponent } from './detail-user-list/detail-user-list.component';
import { DetailUserViewComponent } from './detail-user-view/detail-user-view.component';
import { DetailUserUpdateComponent } from './detail-user-update/detail-user-update.component';
import { DetailUserRoutingModule } from './detail-user-routing.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MainDetailUserComponent } from './main-detail-user/main-detail-user.component';
import { AutoCompleteModule } from 'primeng/autocomplete';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule } from '@angular/material-moment-adapter';
import { ConfirmDialogModule } from 'primeng/confirmdialog';


@NgModule({
  declarations: [
    DetailUserCreateComponent, 
    DetailUserListComponent, 
    DetailUserViewComponent, 
    DetailUserUpdateComponent,
    MainDetailUserComponent,
  ],

  imports: [
    CommonModule,
    DetailUserRoutingModule,
    ReactiveFormsModule,
    AutoCompleteModule,
    FontAwesomeModule,
    MatDatepickerModule,
    MatMomentDateModule,
    ConfirmDialogModule

  ],

})
export class DetailUserModule { }
